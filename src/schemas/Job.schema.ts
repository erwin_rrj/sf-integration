import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema()
export class Job {
  @Prop({ unique: true, required: true })
  id: string;

  @Prop({ required: true })
  type: string;

  @Prop({ required: true })
  name: string;

  @Prop({ required: true })
  site: string;
  due_date: string;

  @Prop({ required: true })
  status: string;

  @Prop({ required: true })
  assignees: string;

  @Prop({ required: true })
  job_class: string;
}

export const JobSchema = SchemaFactory.createForClass(Job);
