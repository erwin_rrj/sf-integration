// salesforce.service.ts

import { Injectable } from '@nestjs/common';
import * as jsforce from 'jsforce';

@Injectable()
export class SalesforceService {
  private conn: jsforce.Connection;
  private accessToken: string;
  private instanceUrl: string;

  constructor() {
    this.authenticate(
      'erwin@kitfoxdesign.com',
      'Y6rr9hb5f@SNYVtKxC5iVIQO3annqIacZZ',
    )
      .then(({ accessToken, instanceUrl }: any) => {
        console.log('Access Token:', accessToken);
        console.log('Instance URL:', instanceUrl);
        this.accessToken = accessToken;
        this.instanceUrl = instanceUrl;
      })
      .catch((error) => {
        console.error('Authentication failed:', error);
      });
  }

  authenticate(username, passwordWithToken) {
    return new Promise((resolve, reject) => {
      // Create a new connection instance
      const conn = new jsforce.Connection();

      // Login using username and password with security token
      conn.login(username, passwordWithToken, function (err, userInfo) {
        if (err) {
          reject(err);
        } else {
          // Store accessToken and instanceUrl for subsequent API calls
          resolve({
            accessToken: conn.accessToken,
            instanceUrl: conn.instanceUrl,
          });
        }
      });
    });
  }

  private connect() {
    try {
      this.conn = new jsforce.Connection({
        instanceUrl: this.instanceUrl,
        accessToken: this.accessToken,
      });
      console.log('Connected to Salesforce with accessToken');
      return this.conn;
    } catch (error) {
      console.error('Error occurred while connecting to Salesforce:', error);
      throw error;
    }
  }

  // Example method to create a Salesforce record
  async createRecord(sObjectName: string, recordData: any): Promise<any> {
    try {
      const result = await this.connect()
        .sobject(sObjectName)
        .create(recordData);
      return result;
    } catch (error) {
      console.error('Error occurred while creating Salesforce record:', error);
      throw error;
    }
  }

  async getAllRecords(sObjectName: string): Promise<any[]> {
    try {
      const records = await this.connect().query(
        `SELECT Id, AccountId, Description, Priority, StartDate, Status, WorkOrderNumber, WorkTypeId FROM ${sObjectName} LIMIT 100`,
      );
      return records.records;
    } catch (error) {
      console.error(
        `Error occurred while retrieving all ${sObjectName} records:`,
        error,
      );
      throw error;
    }
  }

  // Example method to retrieve a Salesforce record by Id
  async getRecordById(sObjectName: string, recordId: string): Promise<any> {
    try {
      const result = await this.conn.sobject(sObjectName).retrieve(recordId);
      return result;
    } catch (error) {
      console.error(
        'Error occurred while retrieving Salesforce record:',
        error,
      );
      throw error;
    }
  }

  // Example method to update a Salesforce record
  async updateRecord(
    sObjectName: string,
    recordId: string,
    updatedData: any,
  ): Promise<any> {
    try {
      const result = await this.conn.sobject(sObjectName).update({
        Id: recordId,
        ...updatedData,
      });
      return result;
    } catch (error) {
      console.error('Error occurred while updating Salesforce record:', error);
      throw error;
    }
  }

  // Example method to delete a Salesforce record
  async deleteRecord(sObjectName: string, recordId: string): Promise<any> {
    try {
      const result = await this.conn.sobject(sObjectName).destroy(recordId);
      return result;
    } catch (error) {
      console.error('Error occurred while deleting Salesforce record:', error);
      throw error;
    }
  }
}
