import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Job, JobSchema } from '../schemas/Job.schema';
import { JobsService } from './jobs.service';
import { JobsController } from './jobs.controller';
import { SalesforceService } from 'src/sf/salesforce.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: Job.name,
        schema: JobSchema,
      },
    ]),
  ],
  controllers: [JobsController],
  providers: [JobsService, SalesforceService],
})
export class JobsModule {}
