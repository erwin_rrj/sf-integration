import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Job } from '../schemas/Job.schema';
import { CreateJobDto } from './dto/create-job.dto';
import { UpdateJobDto } from './dto/update-job.dto';
import { Model } from 'mongoose';

@Injectable()
export class JobsService {
  constructor(@InjectModel(Job.name) private jobModel: Model<Job>) {}

  async create(createJobDto: CreateJobDto) {
    const newJob = new this.jobModel(createJobDto);
    return newJob.save();
  }

  findAll() {
    return this.jobModel.find();
  }

  findOne(id: string) {
    return this.jobModel.findById(id);
  }

  update(id: string, updateJobDto: UpdateJobDto) {
    return this.jobModel.findByIdAndUpdate(id, updateJobDto, {
      new: true,
    });
  }

  remove(id: string) {
    return this.jobModel.findByIdAndDelete(id);
  }
}
