import { IsBoolean, IsOptional, IsString } from 'class-validator';

export class CreateJobDto {
  @IsOptional()
  @IsString()
  id?: string;

  @IsOptional()
  @IsString()
  type?: string;

  @IsOptional()
  @IsString()
  name?: string;

  @IsOptional()
  @IsString()
  site?: string;
  @IsOptional()
  @IsString()
  due_date?: string;

  @IsOptional()
  @IsString()
  status?: string;

  @IsOptional()
  @IsString()
  assignees?: string;

  @IsOptional()
  @IsString()
  job_class?: string;

  @IsOptional()
  @IsBoolean()
  salesforce_saved?: boolean;
}
