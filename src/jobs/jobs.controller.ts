import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpException,
} from '@nestjs/common';
import { JobsService } from './jobs.service';
import { CreateJobDto } from './dto/create-job.dto';
import { UpdateJobDto } from './dto/update-job.dto';
import mongoose from 'mongoose';
import { SalesforceService } from 'src/sf/salesforce.service';

@Controller('jobs')
export class JobsController {
  constructor(
    private readonly jobsService: JobsService,
    private readonly salesforceService: SalesforceService,
  ) {}

  @Post()
  async create(@Body() createJobDto: CreateJobDto) {
    createJobDto.salesforce_saved = false;
    const createdJob = await this.jobsService.create(createJobDto);
    const sObjectName = 'WorkOrder';
    const workOrderData = {
      Subject: createJobDto.type,
      Description: createJobDto.name,
    };
    const createdWorkOrder = await this.salesforceService.createRecord(
      sObjectName,
      workOrderData,
    );
    if (createdWorkOrder.success) {
      createJobDto.salesforce_saved = true;
    }
    console.log('createdWorkOrder', createdWorkOrder);
    return createdJob;
  }

  @Get()
  findAll() {
    return this.jobsService.findAll();
  }

  @Get('salesforce')
  findAllSF() {
    return this.salesforceService.getAllRecords('workOrder');
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.jobsService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateJobDto: UpdateJobDto) {
    return this.jobsService.update(+id, updateJobDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.jobsService.remove(id);
  }
}
