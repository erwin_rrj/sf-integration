import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello() {
    return 'Hello World';
  }
  @Get('me')
  JustAFunction() {
    return 'Erwin was here';
  }
}
