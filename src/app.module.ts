import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { DogsModule } from './dogs/dogs.module';
import { UsersModule } from './users/users.module';
import { PostsModule } from './posts/posts.module';
import { JobsModule } from './jobs/jobs.module';

@Module({
  imports: [
    DogsModule,
    UsersModule,
    PostsModule,
    MongooseModule.forRoot(
      'mongodb+srv://erwinrodriguez:MxZWuyo2vW4PN90H@sf-integration.nl3o25r.mongodb.net/?retryWrites=true&w=majority',
    ),
    JobsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
